#include <stdio.h>
int input()
{
    int x;
    scanf("%d",&x);
    return(x);
}
int calculate(int h,int m)
{
    int tim=(h*60)+m;
    return(tim);
}
void display(int n)
{
    printf("Time in minutes: %d minutes \n",n);
}
int main()
{
    printf("Enter time in hours and minutes \n");
    int h1=input();
    int m1=input();
    int t=calculate(h1,m1);
    display(t);
    return 0;
}
