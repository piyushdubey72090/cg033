#include <stdio.h>
struct student
{
	int rno;
	char name[20];
	char section[5];
	char department[10];
	float fees;
	float result;
};
int main()
{
	int n,max,p;
	printf("Enter number of students\n");
	scanf("%d",&n);
	struct student s[n];
	for(int i=0;i<n;i++)
	{
		printf("Enter roll number, name, section, department, fees, result\n");
		scanf("%d%s%s%s%f%f",&s[i].rno,s[i].name,s[i].section,s[i].department,&s[i].fees,&s[i].result);
	}
	max=s[0].result;
	for(int i=0;i<n;i++)
		if(max<s[i].result)
			p=i;
	printf("Highest scoring student: %s\n roll no.: %d\n section:%s\n dept:%s\n fees:%f\n result:%f\n",s[p].name,s[p].rno,s[p].section,s[p].department,s[p].fees,s[p].result);
	return 0;
}