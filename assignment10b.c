#include<stdio.h>
int main()
{
    int m,n;
    printf("Enter dimension of the two matrix\n");
    scanf("%d%d",&m,&n);
    int a[m][n],b[m][n],sum[m][n],diff[m][n];
    printf("Enter elements of first array:\n");
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
        {
            scanf("%d",&a[i][j]);
        }
    printf("Enter elements of second array:\n");
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
        {
            scanf("%d",&b[i][j]);
        }
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
            sum[i][j]=a[i][j]+b[i][j]; 
    printf("Sum of the two matrices: \n");
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
            printf("\t%d",sum[i][j]);
        printf("\n");
    }
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
            diff[i][j]=a[i][j]-b[i][j];
    printf("Difference of the two matrices: \n");
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
            printf("\t%d",diff[i][j]);
        printf("\n");
    }
    return 0;
}
