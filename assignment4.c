#include <stdio.h>
float input()
{
    float x;
    printf("Enter the radius of circle \n");
    scanf("%f",&x);
    return(x);
}
float calculate(float r)
{
    float area=(22*r*r)/7;
    return(area);
}
void display(float n)
{
    printf("Area of the circle: %f\n",n);
}
int main()
{
    float r=input();
    float arr=calculate(r);
    display(arr);
    return 0;
}
