#include<stdio.h>
int main()
{
    int n,m,i,j;
    printf("enter the dimension of the 2D array \n");
    scanf("%d%d",&m,&n);
    int a[m][n];
    printf("enter the array elements \n");
    for(i=0;i<m;i++)
        for(j=0;j<n;j++)
            scanf("%d",&a[i][j]);
    printf("the 2D array is as follows: \n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
    return 0;
}