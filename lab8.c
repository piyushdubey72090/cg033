#include <stdio.h>
void swap(int *a,int *b)
{
	int t;
	t=*a;
	*a=*b;
	*b=t;
}
int main()
{
	int x,y;
	printf("enter two numbers a and b\n");
	scanf("%d%d",&x,&y);
	swap(&x,&y);
	printf("value a and b after swapping: %d & %d\n",x,y);
	return 0;
}